/* Modules */
const webpack = require('webpack');
const path = require('path');
const MiniCssExtract = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const autoprefixer = require('autoprefixer');
const svgDir = path.resolve(__dirname, '/assets');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');

module.exports = {
  entry: {
    'app': './modules/index.js',
    'home': './modules/home/index.js',
    'catalog': './modules/catalog/index.js',
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, './build'),
    publicPath: './',
  },
  module: {
    rules: [
      {
        test: /\.(m?j|t)s$/,
        include: path.resolve(__dirname, '/component'),
        use: [
          {
            loader: 'babel-loader',
          },
        ],
      },

      {
        test: /\.(scss|css)?$/,
        use: [
          MiniCssExtract.loader,
          {
            loader: 'css-loader',
            options: {
              // minimize: true,
              // importLoaders: 2,
              sourceMap: true,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: [
                autoprefixer({
                  browsers: [
                    'Explorer >= 11',
                    'Edge >= 16',
                    'Firefox >= 52',
                    'Chrome >= 49',
                    'Opera >= 36',
                    'Safari >= 9',
                    '> 0.5% in RU',
                  ],
                }),
              ],
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.(woff(2)?)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: './assets/fonts/[name].[ext]',
              publicPath: '/',
              esModule: false
            },
          },
        ],
      },
      // {
      //   test: /\.(png|jpg|gif)$/,
      //   use: [
      //     {
      //       loader: 'file-loader',
      //       options: {
      //         name: '../assets/images/[name].[ext]',
      //         publicPath: '/',
      //       },
      //     },
      //   ],
      // },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'svg-sprite-loader',
            options: {
              extract: true,
              spriteFilename: './assets/images/icons-sprite.svg',
              runtimeCompat: true,
              esModule: false,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(['./build/']),
    new MiniCssExtract({
      filename: '[name].css',
    }),
    new SpriteLoaderPlugin(),
  ],
};
