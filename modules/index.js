import "../_src/style.scss"
import "../component/Header/Header";
import "../component/Tab/tab";
import "../component/Footer/footer.scss";


function requireAll(r) {
  r.keys().forEach(r);
}
requireAll(require.context('../assets/images/icons/', true, /\.svg$/));
