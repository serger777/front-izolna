import "./slider.scss"
import 'swiper/swiper.scss'
import {Lazy, Navigation, Pagination, Swiper} from 'swiper/js/swiper.esm';

Swiper.use([Navigation, Pagination,Lazy]);


new Swiper(`#swiper-top`, {
    loop: true,
    slidesPerView: 1,
    speed: 500,
    preloadImages: false,
    lazyLoading: true,
    lazy: {
      loadPrevNext: true,
    },
    autoplay: {
      delay: 5000,
    },
    fadeEffect: {
      crossFade: true
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
      disabledClass: 'swiper-button-disabled',
      hiddenClass: 'swiper-button-hidden'
    },
    // breakpoints: {
    //     // when window width is >= 320px
    //     320: {
    //         slidesPerView: 1,
    //         spaceBetween: 20
    //     },
    //     // when window width is >= 480px
    //     480: {
    //         slidesPerView: 1,
    //     },
    //     // when window width is >= 640px
    //     640: {
    //         slidesPerView: 1,
    //     }
    // },
    // pagination: {
    //     clickable: true,
    // },
  })
