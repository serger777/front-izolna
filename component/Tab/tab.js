import './tab.scss'

const tab = document.querySelector(".tab");
const btns = tab.querySelectorAll(".tab__title");
const items = tab.querySelectorAll(".tab__item");
btns.forEach(btn=>{
  btn.addEventListener('click', (e)=>{
    const {target} = e;
    items.forEach(item=>{
      if(target.dataset.target ===item.id){
          item.classList.add("active");
          target.classList.add("active");
      }else{
          item.classList.remove("active");
          btns.forEach(btn=>{
            if(btn.dataset.target === item.id){
              btn.classList.remove("active");
            }
          })
      }
    })
  });
})

