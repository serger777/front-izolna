<?php

use yii\helpers\Html;
use izolna\site\models\helpers\ImgHelper;

/** @var \yii\web\View $this */

\izolna\site\assets\HomeAsset::register($this);
?>
<div class="container">
    <div class="promo">
        <div class="slider" id="swiper-top">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <a href="/" class="swiper-slide__title">Новые поступления льняных тканей</a>
                    <img data-src="<?= ImgHelper::img('slider.jpg') ?>" class="swiper-lazy">
                    <div class="swiper-lazy-preloader"></div>
                    <a href="/" class="btn-link">
                        <span>Смотреть</span>
                        <svg class="icon">
                            <use xlink:href="<?= ImgHelper::icon('btn_arrow') ?>"></use>
                        </svg>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="/" class="swiper-slide__title">Новые поступления льняных тканей</a>
                    <img data-src="<?= ImgHelper::img('slider2.jpg') ?>" class="swiper-lazy">
                    <div class="swiper-lazy-preloader"></div>
                    <a href="/" class="btn-link">
                        <span>Смотреть</span>
                        <svg class="icon">
                            <use xlink:href="<?= ImgHelper::icon('btn_arrow') ?>"></use>
                        </svg>
                    </a>
                </div>
            </div>
            <button class="swiper-button swiper-button-prev">
                <svg class="icon">
                    <use xlink:href="<?= ImgHelper::icon('arrow_left') ?>"></use>
                </svg>
            </button>
            <button class="swiper-button swiper-button-next">
                <svg class="icon">
                    <use xlink:href="<?= ImgHelper::icon('arrow_right') ?>"></use>
                </svg>
            </button>
        </div>
        <div class="wrapper-banner">
            <a href="/" class="banner">
                <h3>Многоразовые маски
                    лен-хлопок</h3>
                <img src="<?= ImgHelper::img('banner2.jpg') ?>" alt="лен">
                <button class="btn-link">
                    <span>Смотреть</span>
                    <svg class="icon">
                        <use xlink:href="<?= ImgHelper::icon('btn_arrow') ?>"></use>
                    </svg>
                </button>
            </a>
            <a href="/" class="banner">
                <h3>Ткани для многоразовых масок</h3>
                <img src="<?= ImgHelper::img('banner3.jpg') ?>" alt="лен">
                <button href="/" class="btn-link">
                    <span>Смотреть</span>
                    <svg class="icon">
                        <use xlink:href="<?= ImgHelper::icon('btn_arrow') ?>"></use>
                    </svg>
                </button>
            </a>
        </div>

    </div>
</div>
<div class="container">
    <h2 class="title">Льняная одежда</h2>
    <div class="tab">
        <div class="tab__header">
            <button type="button" data-target="tab1" class="tab__title">Женская</button>
            <button type="button" data-target="tab2" class="tab__title active">Мужская</button>
        </div>
        <div class="tab__wrapper">
            <div class="tab__item " id="tab1">
                <div class="slider slider-promo" id="swiper-promo-1">
                    <div class="swiper-wrapper">
                        <a href="" class="swiper-slide">
                            <img data-src="<?= ImgHelper::img('dress.jpg') ?>" class="swiper-lazy">
                            <div class="swiper-lazy-preloader"></div>
                            <button type="button" class="btn-link-promo">
                                <span>Платья</span>
                            </button>
                        </a>
                        <a href="" class="swiper-slide">
                            <img data-src="<?= ImgHelper::img('dress2.jpg') ?>" class="swiper-lazy">
                            <div class="swiper-lazy-preloader"></div>
                            <button type="button" class="btn-link-promo">
                                <span>Юбки</span>
                            </button>
                        </a>
                        <a href="" class="swiper-slide">
                            <img data-src="<?= ImgHelper::img('shorts.jpg') ?>" class="swiper-lazy">
                            <div class="swiper-lazy-preloader"></div>
                            <button type="button" class="btn-link-promo">
                                <span>Брюки</span>
                            </button>
                        </a>
                        <a href="" class="swiper-slide">
                            <img data-src="<?= ImgHelper::img('tunic.jpg') ?>" class="swiper-lazy">
                            <div class="swiper-lazy-preloader"></div>
                            <button type="button" class="btn-link-promo">
                                <span>Туники</span>
                            </button>
                        </a>
                        <div href="" class="swiper-slide">
                            <img data-src="<?= ImgHelper::img('shorts.jpg') ?>" class="swiper-lazy">
                            <div class="swiper-lazy-preloader"></div>
                            <button type="button" class="btn-link-promo">
                                <span>Брюки</span>
                            </button>
                        </div>
                    </div>
                    <button class="swiper-button swiper-button-prev">
                        <svg class="icon">
                            <use xlink:href="<?= ImgHelper::icon('arrow_left') ?>"></use>
                        </svg>
                    </button>
                    <button class="swiper-button swiper-button-next">
                        <svg class="icon">
                            <use xlink:href="<?= ImgHelper::icon('arrow_right') ?>"></use>
                        </svg>
                    </button>
                </div>
            </div>
            <div class="tab__item active" id="tab2">
                <div class="slider slider-promo" id="swiper-promo-2">
                    <div class="swiper-wrapper">
                        <a href="" class="swiper-slide">
                            <img data-src="<?= ImgHelper::img('dress.jpg') ?>" class="swiper-lazy">
                            <div class="swiper-lazy-preloader"></div>
                            <button type="button" class="btn-link-promo">
                                <span>Рубаха</span>
                            </button>
                        </a>
                        <a href="" class="swiper-slide">
                            <img data-src="<?= ImgHelper::img('dress2.jpg') ?>" class="swiper-lazy">
                            <div class="swiper-lazy-preloader"></div>
                            <button type="button" class="btn-link-promo">
                                <span>Трусы</span>
                            </button>
                        </a>
                        <a href="" class="swiper-slide">
                            <img data-src="<?= ImgHelper::img('shorts.jpg') ?>" class="swiper-lazy">
                            <div class="swiper-lazy-preloader"></div>
                            <button type="button" class="btn-link-promo">
                                <span>Брюки</span>
                            </button>
                        </a>
                        <a href="" class="swiper-slide">
                            <img data-src="<?= ImgHelper::img('tunic.jpg') ?>" class="swiper-lazy">
                            <div class="swiper-lazy-preloader"></div>
                            <button type="button" class="btn-link-promo">
                                <span>Туники</span>
                            </button>
                        </a>
                        <div href="" class="swiper-slide">
                            <img data-src="<?= ImgHelper::img('shorts.jpg') ?>" class="swiper-lazy">
                            <div class="swiper-lazy-preloader"></div>
                            <button type="button" class="btn-link-promo">
                                <span>Брюки</span>
                            </button>
                        </div>
                    </div>
                    <button class="swiper-button swiper-button-prev">
                        <svg class="icon">
                            <use xlink:href="<?= ImgHelper::icon('arrow_left') ?>"></use>
                        </svg>
                    </button>
                    <button class="swiper-button swiper-button-next">
                        <svg class="icon">
                            <use xlink:href="<?= ImgHelper::icon('arrow_right') ?>"></use>
                        </svg>
                    </button>
                </div>
            </div>
        </div>

    </div>

</div>
<div class="container">
    <div class="advantages">
        <div class="advantages__item">
            <svg class="icon">
                <use xlink:href="<?= ImgHelper::icon('showroom') ?>"></use>
            </svg>
            <p class="advantages__title">Магазины в Москве и Костроме</p>
            <p class="advantages__text">Здесь можно забрать оформленный заказ, а также посмотреть образцы тканей и штор. Москва: м. Кузнецкий мост ул. Рождественка 5/7. Кострома: ул. Молочная гора 2/1 </p>
        </div>
        <div class="advantages__item">
            <svg class="icon">
                <use xlink:href="<?= ImgHelper::icon('sewingmachine') ?>"></use>
            </svg>
            <p class="advantages__title">Собственное производство</p>
            <p class="advantages__text">Располагаем современным оборудованием и используем высококачественные материалы для изготовления уютных и натуральных предметов интерьера для Вашего дома</p>
        </div>
        <div class="advantages__item">
            <svg class="icon">
                <use xlink:href="<?= ImgHelper::icon('cloth') ?>"></use>
            </svg>
            <p class="advantages__title">Пошив столового белья, льняныч штор</p>
            <p class="advantages__text">Мы изготавливаем скатерти, салфетки, простыни, покрывала, наволочки, шторы на собственном производстве</p>
        </div>
        <div class="advantages__item">
            <svg class="icon">
                <use xlink:href="<?= ImgHelper::icon('calculator') ?>"></use>
            </svg>
            <p class="advantages__title">Онлайн расчет услуг и материалов</p>
            <p class="advantages__text">Можете рассчитать необходимое количество ткани и узнать сроки и стоимость доставки готового изделия</p>
        </div>
    </div>
</div>
<div class="container">
    <h2 class="title">Ткани льняные</h2>
    <div class="slider slider-promo" id="swiper-promo-3">
        <div class="swiper-wrapper">
            <a href="" class="swiper-slide">
                <img data-src="<?= ImgHelper::img('cloth_closes.jpg') ?>" class="swiper-lazy">
                <div class="swiper-lazy-preloader"></div>
                <button type="button" class="btn-link-promo">
                    <span>Ткани льняные</span>
                </button>
            </a>
            <a href="" class="swiper-slide">
                <img data-src="<?= ImgHelper::img('cloth_width.jpg') ?>" class="swiper-lazy">
                <div class="swiper-lazy-preloader"></div>
                <button type="button" class="btn-link-promo">
                    <span>Широкие</span>
                </button>
            </a>
            <a href="" class="swiper-slide">
                <img data-src="<?= ImgHelper::img('cloth_decor.jpg') ?>" class="swiper-lazy">
                <div class="swiper-lazy-preloader"></div>
                <button type="button" class="btn-link-promo">
                    <span>Декоративные</span>
                </button>
            </a>
            <a href="" class="swiper-slide">
                <img data-src="<?= ImgHelper::img('cloth_paint.jpg') ?>" class="swiper-lazy">
                <div class="swiper-lazy-preloader"></div>
                <button type="button" class="btn-link-promo">
                    <span>С рисунком</span>
                </button>
            </a>
        </div>
        <button class="swiper-button swiper-button-prev">
            <svg class="icon">
                <use xlink:href="<?= ImgHelper::icon('arrow_left') ?>"></use>
            </svg>
        </button>
        <button class="swiper-button swiper-button-next">
            <svg class="icon">
                <use xlink:href="<?= ImgHelper::icon('arrow_right') ?>"></use>
            </svg>
        </button>
    </div>
</div>
<div class="container">
    <h2 class="title">Товары для дома</h2>
    <div class="slider slider-promo" id="swiper-promo-4">
        <div class="swiper-wrapper">
            <a href="" class="swiper-slide">
                <img data-src="<?= ImgHelper::img('home_slide_1.jpg') ?>" class="swiper-lazy">
                <div class="swiper-lazy-preloader"></div>
                <button type="button" class="btn-link-promo">
                    <span>Скатерти</span>
                </button>
            </a>
            <a href="" class="swiper-slide">
                <img data-src="<?= ImgHelper::img('home_slide_2.jpg') ?>" class="swiper-lazy">
                <div class="swiper-lazy-preloader"></div>
                <button type="button" class="btn-link-promo">
                    <span>Постельное бельё</span>
                </button>
            </a>
            <a href="" class="swiper-slide">
                <img data-src="<?= ImgHelper::img('home_slide3.jpg') ?>" class="swiper-lazy">
                <div class="swiper-lazy-preloader"></div>
                <button type="button" class="btn-link-promo">
                    <span>Полотенца</span>
                </button>
            </a>
            <a href="" class="swiper-slide">
                <img data-src="<?= ImgHelper::img('home_slide4.jpg') ?>" class="swiper-lazy">
                <div class="swiper-lazy-preloader"></div>
                <button type="button" class="btn-link-promo">
                    <span>Салфетки льняные </span>
                </button>
            </a>
        </div>
        <button class="swiper-button swiper-button-prev">
            <svg class="icon">
                <use xlink:href="<?= ImgHelper::icon('arrow_left') ?>"></use>
            </svg>
        </button>
        <button class="swiper-button swiper-button-next">
            <svg class="icon">
                <use xlink:href="<?= ImgHelper::icon('arrow_right') ?>"></use>
            </svg>
        </button>
    </div>
</div>



