<?php

use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var string $name */
/** @var string $message */
/** @var \Exception $exception */

try {
    echo $this->render($exception->statusCode, [
        'name' => $name,
        'message' => $message,
        'exception' => $exception,
    ]);
} catch (\Exception $e) {
    echo $this->render('default', [
        'name' => $name,
        'message' => $message,
        'exception' => $exception,
    ]);
}