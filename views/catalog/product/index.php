<?php

use yii\helpers\Html;
use izolna\site\models\helpers\ImgHelper;
use yii\web\View;

/** @var View $this */

\izolna\site\assets\catalog\ProductAsset::register($this);
?>
<div class="container">
    <div class="filter">

    </div>
    <div class="products">
            <div class="product">
                <img src="<?= ImgHelper::img('product-item.jpg') ?>" alt="Платье льняное Матера">
                <p class="product__name">
                    Платье льняное Матера
                </p>
                <div class="color">
                    <div class="color__text">Цвет: <span>меланж</span></div>
                    <div class="color__wrap">
                        <div class="color__item-blue"></div>
                        <div class="color__item-white"></div>
                        <div class="color__item-orange"></div>
                        <div class="color__item-red"></div>
                    </div>
                </div>
                <div class="product__price">
                    от 4 690 ₽
                </div>
            </div>
        </div>
</div>


