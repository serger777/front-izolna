<?php

use yii\helpers\Html;
use izolna\site\models\helpers\ImgHelper;
use yii\web\View;

/** @var View $this */

\izolna\site\assets\CatalogAsset::register($this);
?>
<div class="container">
    <div class="products">
        <div class="filter">

        </div>
        <div class="products__list">
            <div class="product">
                <a href="/">
                    <img class="product__img" src="<?= ImgHelper::img('product-item.jpg') ?>" alt="Платье льняное Матера">
                </a>
                <div class="product__main">
                    <p class="product__name">
                        Платье льняное Матера
                    </p>
                    <div class="color">
                        <div class="color__text">Цвет: <span>меланж</span></div>
                        <div class="color__wrap">
                            <div class="color__item color__item--blue"></div>
                            <div class=" color__item color__item--white"></div>
                            <div class="color__item color__item--orange"></div>
                            <div class="color__item color__item--red"></div>
                        </div>
                    </div>
                    <div class="product__price">
                        от 4 690 ₽
                    </div>
                </div>
                <div class="basket-add">
                    <button class="basket-add__link button">В корзину</button>
                </div>
            </div>
            <div class="product">
                <a href="/">
                    <img class="product__img" src="<?= ImgHelper::img('product-item.jpg') ?>" alt="Платье льняное Матера">
                </a>
                <div class="product__main">
                    <p class="product__name">
                        Платье льняное Матера
                    </p>
                    <div class="color">
                        <div class="color__text">Цвет: <span>меланж</span></div>
                        <div class="color__wrap">
                            <div class="color__item color__item--blue"></div>
                            <div class=" color__item color__item--white"></div>
                            <div class="color__item color__item--orange"></div>
                            <div class="color__item color__item--red"></div>
                        </div>
                    </div>
                    <div class="product__price">
                        от 4 690 ₽
                    </div>
                </div>
                <div class="basket-add">
                    <button class="basket-add__link button">В корзину</button>
                </div>
            </div>
            <div class="product">
                <a href="/products">
                    <img class="product__img" src="<?= ImgHelper::img('product-item.jpg') ?>" alt="Платье льняное Матера">
                </a>
                <div class="product__main">
                    <p class="product__name">
                        Платье льняное Матера
                    </p>
                    <div class="color">
                        <div class="color__text">Цвет: <span>меланж</span></div>
                        <div class="color__wrap">
                            <div class="color__item color__item--blue"></div>
                            <div class=" color__item color__item--white"></div>
                            <div class="color__item color__item--orange"></div>
                            <div class="color__item color__item--red"></div>
                        </div>
                    </div>
                    <div class="product__price">
                        от 4 690 ₽
                    </div>
                </div>
                <div class="basket-add">
                    <button class="basket-add__link button">В корзину</button>
                </div>
            </div>
            <div class="product">
                <a href="/products">
                    <img class="product__img" src="<?= ImgHelper::img('product-item.jpg') ?>" alt="Платье льняное Матера">
                </a>
                <div class="product__main">
                    <p class="product__name">
                        Платье льняное Матера
                    </p>
                    <div class="color">
                        <div class="color__text">Цвет: <span>меланж</span></div>
                        <div class="color__wrap">
                            <div class="color__item color__item--blue"></div>
                            <div class=" color__item color__item--white"></div>
                            <div class="color__item color__item--orange"></div>
                            <div class="color__item color__item--red"></div>
                        </div>
                    </div>
                    <div class="product__price">
                        от 4 690 ₽
                    </div>
                </div>
                <div class="basket-add">
                    <button class="basket-add__link button">В корзину</button>
                </div>
            </div>
        </div>
    </div>
</div>


