<?php

use yii\helpers\Html;
use izolna\site\models\helpers\ImgHelper;

/** @var \yii\web\View $this */

?>
<header class="header">
    <div class="header-top">
        <div class="container">
            <div class="header-top__wrap">
                <div class="header-top__left">
                    <a href="" class="city header-top__item">
                        <svg class="icon">
                            <use xlink:href="<?=ImgHelper::icon('location') ?>"></use>
                        </svg>
                        <span>Ваш город</span>
                    </a>
                    <a href="" class="city header-top__item">
                        <svg class="icon">
                            <use xlink:href="<?=ImgHelper::icon('storecity') ?>"></use>
                        </svg>
                        <span>Магазины</span>
                    </a>
                </div>
                <div class="header-top__right">
                    <a href="" class="city header-top__item">
                        <svg class="icon">
                            <use xlink:href="<?=ImgHelper::icon('pointdelivery') ?>"></use>
                        </svg>
                        <span>Ваш город</span>
                    </a>
                    <a href="" class="city header-top__item">
                        <svg class="icon">
                            <use xlink:href="<?=ImgHelper::icon('delivery') ?>"></use>
                        </svg>
                        <span>Доставка и оплата</span>
                    </a>
                    <a href="" class="city header-top__item">
                        <svg class="icon">
                            <use xlink:href="<?=ImgHelper::icon('help') ?>"></use>
                        </svg>
                        <span>Помощь</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="header-main">
            <div class="header-main__left">
                <button class="phone-call header-main__item">
                    <svg class="icon">
                        <use xlink:href="<?=ImgHelper::icon('phone') ?>"></use>
                    </svg>
                    <span>Обратный звонок</span>
                </button>
                <a class="phone-link header-main__item" href="tel:8-800-600-49-46">
                    <span> 8-800-600-49-46</span>
                </a>
                <a class="phone-link header-main__item" href="tel:+7(495)968-20-64">
                    <span>+7 (495) 968-20-64</span>
                </a>
            </div>
            <a href="" class="logo">
                <img src="<?= ImgHelper::img('logo.svg') ?>" alt="">
            </a>
            <div class="header-main__right">
                <a href="" class="city header-main__item">
                    <svg class="icon">
                        <use xlink:href="<?=ImgHelper::icon('search') ?>"></use>
                    </svg>
                    <span>Поиск</span>
                </a>
                <a href="" class="city header-main__item">
                    <svg class="icon">
                        <use xlink:href="<?=ImgHelper::icon('account') ?>"></use>
                    </svg>
                    <span>Войти</span>
                </a>
                <a href="" class="city header-main__item">
                    <svg class="icon">
                        <use xlink:href="<?=ImgHelper::icon('shop') ?>"></use>
                    </svg>
                    <span>Корзина</span>
                </a>
            </div>
        </div>
    </div>
    <div class="nav-wrap">
        <nav class="nav">
            <div class="nav__item ">
                <a href="" class="nav__link active">
                    Ткани
                </a>
                <ul class="nav-list">
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link">Для одежды</a>
                        <a href="" class="nav-list__link">Широки</a>
                        <a href="" class="nav-list__link">В клетку/Полоску</a>
                        <a href="" class="nav-list__link">С рисунком</a>
                        <a href="" class="nav-list__link">Лен-лавсан</a>
                        <a href="" class="nav-list__link">Лен-хлопок</a>
                        <a href="" class="nav-list__link">Для постельного белья</a>
                        <a href="" class="nav-list__link">Полотенечные ткани</a>
                        <a href="" class="nav-list__link">Новогодние ткани</a>
                        <a href="" class="nav-list__link">Кружево</a>
                        <a href="" class="nav-list__link">Кружево</a>
                    </li>
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link ">Декоративные</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для штор</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для скатерьтей</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для покрывал</a>
                        <a href="" class="nav-list__link nav-list__link--small">Грубый лен</a>
                    </li>
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link">Новинки</a>
                        <a href="" class="nav-list__link">Хит продаж</a>
                        <a href="" class="nav-list__link">Все товары</a>
                    </li>
                    <li class="nav-list__wrap nav-list__image">
                        <img src="<?= ImgHelper::img('menu_images.png') ?>" title="ткани" alt="">
                    </li>

                </ul>
            </div>
            <div class="nav__item">
                <a href="" class="nav__link">
                    Одежда
                </a>
                <ul class="nav-list">
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link">Для такого</a>
                        <a href="" class="nav-list__link">Широки спектр</a>
                        <a href="" class="nav-list__link">В клетку/Полоску</a>
                        <a href="" class="nav-list__link">С рисунком</a>
                        <a href="" class="nav-list__link">Лен-лавсан</a>
                        <a href="" class="nav-list__link">Лен-хлопок</a>
                        <a href="" class="nav-list__link">Для постельного белья</a>
                        <a href="" class="nav-list__link">Полотенечные ткани</a>
                        <a href="" class="nav-list__link">Новогодние ткани</a>
                        <a href="" class="nav-list__link">Кружево</a>
                        <a href="" class="nav-list__link">Кружево</a>
                    </li>
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link ">Декоративные</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для штор</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для скатерьтей</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для покрывал</a>
                        <a href="" class="nav-list__link nav-list__link--small">Грубый лен</a>
                    </li>
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link">Новинки</a>
                        <a href="" class="nav-list__link">Хит продаж</a>
                        <a href="" class="nav-list__link">Все товары</a>
                    </li>
                    <li class="nav-list__wrap nav-list__image">
                        <img src="<?= ImgHelper::img('menu_images.png') ?>" title="ткани" alt="">
                    </li>

                </ul>
            </div>
            <div class="nav__item">
                <a href="" class="nav__link">
                    Товары для дома
                </a>
                <ul class="nav-list">
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link">Для одежды</a>
                        <a href="" class="nav-list__link">Широки</a>
                        <a href="" class="nav-list__link">В клетку/Полоску</a>
                        <a href="" class="nav-list__link">С рисунком</a>
                        <a href="" class="nav-list__link">Лен-лавсан</a>
                        <a href="" class="nav-list__link">Лен-хлопок</a>
                        <a href="" class="nav-list__link">Для постельного белья</a>
                        <a href="" class="nav-list__link">Полотенечные ткани</a>
                        <a href="" class="nav-list__link">Новогодние ткани</a>
                        <a href="" class="nav-list__link">Кружево</a>
                        <a href="" class="nav-list__link">Кружево</a>
                    </li>
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link ">Декоративные</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для штор</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для скатерьтей</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для покрывал</a>
                        <a href="" class="nav-list__link nav-list__link--small">Грубый лен</a>
                    </li>
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link">Новинки</a>
                        <a href="" class="nav-list__link">Хит продаж</a>
                        <a href="" class="nav-list__link">Все товары</a>
                    </li>
                    <li class="nav-list__wrap nav-list__image">
                        <img src="<?= ImgHelper::img('menu-home.jpg') ?>" title="ткани" alt="">
                    </li>

                </ul>
            </div>
            <div class="nav__item">
                <a href="" class="nav__link">
                    Распродажа
                </a>
                <ul class="nav-list">
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link">Для одежды</a>
                        <a href="" class="nav-list__link">Широки</a>
                        <a href="" class="nav-list__link">В клетку/Полоску</a>
                        <a href="" class="nav-list__link">С рисунком</a>
                        <a href="" class="nav-list__link">Лен-лавсан</a>
                        <a href="" class="nav-list__link">Лен-хлопок</a>
                        <a href="" class="nav-list__link">Для постельного белья</a>
                        <a href="" class="nav-list__link">Полотенечные ткани</a>
                        <a href="" class="nav-list__link">Новогодние ткани</a>
                        <a href="" class="nav-list__link">Кружево</a>
                        <a href="" class="nav-list__link">Кружево</a>
                    </li>
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link ">Декоративные</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для штор</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для скатерьтей</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для покрывал</a>
                        <a href="" class="nav-list__link nav-list__link--small">Грубый лен</a>
                    </li>
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link">Новинки</a>
                        <a href="" class="nav-list__link">Хит продаж</a>
                        <a href="" class="nav-list__link">Все товары</a>
                    </li>
                    <li class="nav-list__wrap nav-list__image">
                        <img src="<?= ImgHelper::img('menu-opt.jpg') ?>" title="ткани" alt="">
                    </li>

                </ul>
            </div>
            <div class="nav__item">
                <a href="" class="nav__link">
                    Оптовикам
                </a>
                <ul class="nav-list">
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link">Для одежды</a>
                        <a href="" class="nav-list__link">Широки</a>
                        <a href="" class="nav-list__link">В клетку/Полоску</a>
                        <a href="" class="nav-list__link">С рисунком</a>
                        <a href="" class="nav-list__link">Лен-лавсан</a>
                        <a href="" class="nav-list__link">Лен-хлопок</a>
                        <a href="" class="nav-list__link">Для постельного белья</a>
                        <a href="" class="nav-list__link">Полотенечные ткани</a>
                        <a href="" class="nav-list__link">Новогодние ткани</a>
                        <a href="" class="nav-list__link">Кружево</a>
                        <a href="" class="nav-list__link">Кружево</a>
                    </li>
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link ">Декоративные</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для штор</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для скатерьтей</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для покрывал</a>
                        <a href="" class="nav-list__link nav-list__link--small">Грубый лен</a>
                    </li>
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link">Новинки</a>
                        <a href="" class="nav-list__link">Хит продаж</a>
                        <a href="" class="nav-list__link">Все товары</a>
                    </li>
                    <li class="nav-list__wrap nav-list__image">
                        <img src="<?= ImgHelper::img('menu_images.png') ?>" title="ткани" alt="">
                    </li>

                </ul>
            </div>
            <div class="nav__item">
                <a href="" class="nav__link">
                    Услуги
                </a>
                <ul class="nav-list">
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link">Для одежды</a>
                        <a href="" class="nav-list__link">Широки</a>
                        <a href="" class="nav-list__link">В клетку/Полоску</a>
                        <a href="" class="nav-list__link">С рисунком</a>
                        <a href="" class="nav-list__link">Лен-лавсан</a>
                        <a href="" class="nav-list__link">Лен-хлопок</a>
                        <a href="" class="nav-list__link">Для постельного белья</a>
                        <a href="" class="nav-list__link">Полотенечные ткани</a>
                        <a href="" class="nav-list__link">Новогодние ткани</a>
                        <a href="" class="nav-list__link">Кружево</a>
                        <a href="" class="nav-list__link">Кружево</a>
                    </li>
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link ">Декоративные</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для штор</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для скатерьтей</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для покрывал</a>
                        <a href="" class="nav-list__link nav-list__link--small">Грубый лен</a>
                        <a href="" class="nav-list__link ">Декоративные</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для штор</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для скатерьтей</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для покрывал</a>
                        <a href="" class="nav-list__link nav-list__link--small">Грубый лен</a>
                    </li>
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link">Новинки</a>
                        <a href="" class="nav-list__link">Хит продаж</a>
                        <a href="" class="nav-list__link">Все товары</a>
                    </li>
                    <li class="nav-list__wrap nav-list__image">
                        <img src="<?= ImgHelper::img('menu-services.jpg') ?>" title="ткани" alt="">
                    </li>

                </ul>
            </div>
            <div class="nav__item">
                <a href="" class="nav__link">
                    Блог
                </a>
                <ul class="nav-list">
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link">Для одежды</a>
                        <a href="" class="nav-list__link">Широки</a>
                        <a href="" class="nav-list__link">В клетку/Полоску</a>
                        <a href="" class="nav-list__link">С рисунком</a>
                        <a href="" class="nav-list__link">Лен-лавсан</a>
                        <a href="" class="nav-list__link">Лен-хлопок</a>
                        <a href="" class="nav-list__link">Для постельного белья</a>
                        <a href="" class="nav-list__link">Полотенечные ткани</a>
                        <a href="" class="nav-list__link">Новогодние ткани</a>
                        <a href="" class="nav-list__link">Кружево</a>
                        <a href="" class="nav-list__link">Кружево</a>
                    </li>
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link ">Декоративные</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для штор</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для скатерьтей</a>
                        <a href="" class="nav-list__link nav-list__link--small">Для покрывал</a>
                        <a href="" class="nav-list__link nav-list__link--small">Грубый лен</a>
                    </li>
                    <li class="nav-list__wrap">
                        <a href="" class="nav-list__link">Новинки</a>
                        <a href="" class="nav-list__link">Хит продаж</a>
                        <a href="" class="nav-list__link">Все товары</a>
                    </li>
                    <li class="nav-list__wrap nav-list__image">
                        <img src="<?= ImgHelper::img('closes.png') ?>" title="ткани" alt="">
                    </li>

                </ul>
            </div>
        </nav>
    </div>

</header>
