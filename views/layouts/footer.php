<?php

use yii\helpers\Html;
use izolna\site\models\helpers\ImgHelper;

/** @var \yii\web\View $this */
?>
<footer class="footer">
    <div class="container">
        <div class="wrap-more">
            <p>Самый льняной магазин «ИзоЛьна.ру» - магазин домашнего уюта и гармонии! Здесь собрано все, чтобы
                подчеркнуть тепло и радушие вашего жилища. У нас вы можете купить лён в ваш дом по самым выгодным
                ценам</p>
            <button>Подробнее</button>
        </div>
    </div>
    <div class="footer-top">
        <div class="container">
            <div class="footer-top__wrap">
                <div class="footer-top__left">
                    <h3 class="footer-top__title">Магазины</h3>
                    <p class="footer-top__text">г. Москва, м. Кузнецкий мост, ул. Рождественка, 5/7</p>
                    <p class="footer-top__text">г. Кострома, ул. Молочная гора, 2/1</p>
                </div>
                <div class="footer-top__right">
                    <div class="footer-top__item">
                        <h3 class="footer-top__title">Напишите нам</h3>
                        <a href="mailto:info@izolna.ru">info@izolna.ru</a>
                    </div>
                    <div class="footer-top__item">
                        <h3 class="footer-top__title">Магазины</h3>
                        <button class="phone-call footer-top__link">
                            <svg class="icon">
                                <use xlink:href="<?=ImgHelper::icon('phone') ?>"></use>
                            </svg>
                            <span>Обратный звонок</span>
                        </button>
                        <a class="phone-link footer-top__link" href="tel:8-800-600-49-46">
                            <span> 8-800-600-49-46</span>
                        </a>
                        <a class="phone-link footer-top__link" href="tel:+7(495)968-20-64">
                            <span>+7 (495) 968-20-64</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="footer-nav">
                <div class="footer-nav__item">
                    <a href="" class="footer-nav__link footer-nav__link--bold">О компании</a>
                    <a href="" class="footer-nav__link footer-nav__link--bold">Льняная одежда</a>
                    <a href="" class="footer-nav__link footer-nav__link--bold">Льняные ткани</a>
                    <a href="" class="footer-nav__link footer-nav__link--bold">Товары для дома</a>
                    <a href="" class="footer-nav__link footer-nav__link--bold">Обратный звонок</a>
                    <a href="" class="footer-nav__link footer-nav__link--bold">Карта сайта</a>
                </div>
                <div class="footer-nav__item">
                    <a href="" class="footer-nav__link ">Магазин в Москве</a>
                    <a href="" class="footer-nav__link ">Магазин в Костроме</a>
                    <a href="" class="footer-nav__link ">Производство</a>
                    <a href="" class="footer-nav__link ">Доставка</a>
                    <a href="" class="footer-nav__link ">Наши клиенты</a>
                    <a href="" class="footer-nav__link ">Преимущества</a>
                    <a href="" class="footer-nav__link ">Отзывы</a>
                    <a href="" class="footer-nav__link ">Готовые решения</a>
                    <a href="" class="footer-nav__link ">Пользовательское соглашение</a>
                    <a href="" class="footer-nav__link ">Политика конфиденциальности</a>
                    <a href="" class="footer-nav__link ">Публичная оферта</a>
                    <a href="" class="footer-nav__link ">Онлайн презентация</a>
                </div>
                <div class="footer-nav__item">
                    <a href="" class="footer-nav__link">Как сделать заказ</a>
                    <a href="" class="footer-nav__link">Оплата</a>
                    <a href="" class="footer-nav__link">Доставка</a>
                    <a href="" class="footer-nav__link">Пункты выдачи</a>
                    <a href="" class="footer-nav__link">Гарантии</a>
                    <a href="" class="footer-nav__link">Резерв</a>
                    <a href="" class="footer-nav__link">Обмен и возврат</a>
                    <a href="" class="footer-nav__link">Как померить</a>
                    <a href="" class="footer-nav__link">Типы крепления штор</a>
                    <a href="" class="footer-nav__link">Вопросы и ответы</a>
                    <a href="" class="footer-nav__link">Правила ухода</a>
                </div>
                <div class="footer-nav__item">
                    <div class="pay-wrap">
                        <h4>Мы принимаем к оплате</h4>
                        <div class="pay-wrap__item">
                            <img src="<?= ImgHelper::img('payments/visa.svg') ?>" alt="visa">
                            <img src="<?= ImgHelper::img('payments/payonline.png') ?>" alt="payonline">
                            <img src="<?= ImgHelper::img('payments/paypal.png') ?>" alt="paypal">
                            <img src="<?= ImgHelper::img('payments/mastercard.png') ?>" alt="mastercard">
                            <img src="<?= ImgHelper::img('payments/halva.png') ?>" alt="halva">
                        </div>
                        <a href="" class="footer-nav__link">Подробнее о службах доставки</a>
                    </div>
                    <div class="pay-wrap">
                        <h4>Службы доставки</h4>
                        <div class="pay-wrap__item">
                            <img src="<?= ImgHelper::img('payments/ems.png') ?>" alt="ems">
                            <img src="<?= ImgHelper::img('payments/delovie-linii.png') ?>" alt="деловые линии">
                            <img src="<?= ImgHelper::img('payments/pek.png') ?>" alt="pek">
                            <img src="<?= ImgHelper::img('payments/pochta-rossii.png') ?>" alt="pochta">
                            <img src="<?= ImgHelper::img('payments/sdek.png') ?>" alt="sdek">
                        </div>
                    </div>
                </div>
            </div>
            <p class="copyright">© 2009-2020 — «ИзоЛьна.ру»</p>
        </div>
    </div>
</footer>

