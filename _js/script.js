import '../_src/style.scss'



import { Swiper, Navigation, Pagination, Lazy} from 'swiper/js/swiper.esm.js';




// swiper.forEach(item=>{
//     new Swiper(`#${item.id}`,{
//         loop: true,
//         slidesPerView:1,
//         speed: 500,
//         preloadImages: false,
//         lazyLoading: true,
//         lazy: {
//             loadPrevNext: true,
//         },
//         autoplay: {
//             delay: 5000,
//         },
//         fadeEffect: {
//             crossFade: true
//         },
//         navigation: {
//             nextEl: '.swiper-button-next',
//             prevEl: '.swiper-button-prev',
//             disabledClass: 'swiper-button-disabled',
//             hiddenClass: 'swiper-button-hidden'
//         },
//         breakpoints: {
//             // when window width is >= 320px
//             320: {
//                 slidesPerView: 1,
//                 spaceBetween: 20
//             },
//             // when window width is >= 480px
//             480: {
//                 slidesPerView: 1,
//                 spaceBetween: 30
//             },
//             // when window width is >= 640px
//             640: {
//                 slidesPerView: 1,
//                 spaceBetween: 40
//             }
//         },
//         pagination: {
//             el: '.swiper-pagination',
//             clickable: true,
//             bulletClass: 't-ScrollBar__item',
//             bulletActiveClass: 'is-active'
//         },
//
//     });
// });
const sendForm = ()=>{
    const form = document.querySelector(".form");
    const inputs = form.querySelectorAll("[required]");
    const formSuccess = form.querySelector(".form__success");
    const wrapForm = form.querySelector(".wrap__form");
    const btn = form.querySelector(".button");
    const send=()=>{
        const url = 'https://api.unisender.com/ru/api/subscribe?format=json&api_key=6g9u3a9xgaipdkyqiuoakp5eodbrwqu3ygqd9mto&list_ids=20137861&double_optin=3';
        fetch(url, {
            method: 'POST',
            body: new FormData(form)
        }).then(response=>response.json())
            .then((data)=>{
                console.log(data);
                if(data.result.person_id){
                    formSuccess.classList.remove("disabled");
                    wrapForm.remove()
                }
            }).catch((err)=>{
            console.log(err);
        })
    };
    inputs.forEach(item=>{
        item.addEventListener("keyup",(e)=>{
            if (item.validity.valid){
                item.parentNode.classList.remove("error")
            }else{
                console.log("error");
            }

        })
    });
    btn.addEventListener("click", (e)=>{
        e.preventDefault();
        inputs.forEach(item=>{
            if (item.validity.valid){
                send();
            }else {
                item.parentNode.classList.add("error")
            }
        })

    })
};
// sendForm();


