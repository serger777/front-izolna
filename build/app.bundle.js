/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "./";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./modules/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./_src/style.scss":
/*!*************************!*\
  !*** ./_src/style.scss ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./_src/style.scss?");

/***/ }),

/***/ "./assets/images/icons sync recursive \\.svg$":
/*!*****************************************!*\
  !*** ./assets/images/icons sync \.svg$ ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var map = {\n\t\"./account.svg\": \"./assets/images/icons/account.svg\",\n\t\"./arrow_left.svg\": \"./assets/images/icons/arrow_left.svg\",\n\t\"./arrow_right.svg\": \"./assets/images/icons/arrow_right.svg\",\n\t\"./btn_arrow.svg\": \"./assets/images/icons/btn_arrow.svg\",\n\t\"./burgermenu.svg\": \"./assets/images/icons/burgermenu.svg\",\n\t\"./calculator.svg\": \"./assets/images/icons/calculator.svg\",\n\t\"./check.svg\": \"./assets/images/icons/check.svg\",\n\t\"./close.svg\": \"./assets/images/icons/close.svg\",\n\t\"./cloth.svg\": \"./assets/images/icons/cloth.svg\",\n\t\"./delivery.svg\": \"./assets/images/icons/delivery.svg\",\n\t\"./discount.svg\": \"./assets/images/icons/discount.svg\",\n\t\"./hearth.svg\": \"./assets/images/icons/hearth.svg\",\n\t\"./help.svg\": \"./assets/images/icons/help.svg\",\n\t\"./like.svg\": \"./assets/images/icons/like.svg\",\n\t\"./location.svg\": \"./assets/images/icons/location.svg\",\n\t\"./logo.svg\": \"./assets/images/icons/logo.svg\",\n\t\"./more_h.svg\": \"./assets/images/icons/more_h.svg\",\n\t\"./more_v.svg\": \"./assets/images/icons/more_v.svg\",\n\t\"./online_card.svg\": \"./assets/images/icons/online_card.svg\",\n\t\"./phone.svg\": \"./assets/images/icons/phone.svg\",\n\t\"./point_delivery.svg\": \"./assets/images/icons/point_delivery.svg\",\n\t\"./pointdelivery.svg\": \"./assets/images/icons/pointdelivery.svg\",\n\t\"./search.svg\": \"./assets/images/icons/search.svg\",\n\t\"./sewingmachine.svg\": \"./assets/images/icons/sewingmachine.svg\",\n\t\"./shop.svg\": \"./assets/images/icons/shop.svg\",\n\t\"./showroom.svg\": \"./assets/images/icons/showroom.svg\",\n\t\"./star.svg\": \"./assets/images/icons/star.svg\",\n\t\"./storecity.svg\": \"./assets/images/icons/storecity.svg\"\n};\n\n\nfunction webpackContext(req) {\n\tvar id = webpackContextResolve(req);\n\treturn __webpack_require__(id);\n}\nfunction webpackContextResolve(req) {\n\tif(!__webpack_require__.o(map, req)) {\n\t\tvar e = new Error(\"Cannot find module '\" + req + \"'\");\n\t\te.code = 'MODULE_NOT_FOUND';\n\t\tthrow e;\n\t}\n\treturn map[req];\n}\nwebpackContext.keys = function webpackContextKeys() {\n\treturn Object.keys(map);\n};\nwebpackContext.resolve = webpackContextResolve;\nmodule.exports = webpackContext;\nwebpackContext.id = \"./assets/images/icons sync recursive \\\\.svg$\";\n\n//# sourceURL=webpack:///./assets/images/icons_sync_\\.svg$?");

/***/ }),

/***/ "./assets/images/icons/account.svg":
/*!*****************************************!*\
  !*** ./assets/images/icons/account.svg ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"account-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#account-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/account.svg?");

/***/ }),

/***/ "./assets/images/icons/arrow_left.svg":
/*!********************************************!*\
  !*** ./assets/images/icons/arrow_left.svg ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"arrow_left-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#arrow_left-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/arrow_left.svg?");

/***/ }),

/***/ "./assets/images/icons/arrow_right.svg":
/*!*********************************************!*\
  !*** ./assets/images/icons/arrow_right.svg ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"arrow_right-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#arrow_right-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/arrow_right.svg?");

/***/ }),

/***/ "./assets/images/icons/btn_arrow.svg":
/*!*******************************************!*\
  !*** ./assets/images/icons/btn_arrow.svg ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"btn_arrow-usage\",\n      viewBox: \"0 0 19 19\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#btn_arrow-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/btn_arrow.svg?");

/***/ }),

/***/ "./assets/images/icons/burgermenu.svg":
/*!********************************************!*\
  !*** ./assets/images/icons/burgermenu.svg ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"burgermenu-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#burgermenu-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/burgermenu.svg?");

/***/ }),

/***/ "./assets/images/icons/calculator.svg":
/*!********************************************!*\
  !*** ./assets/images/icons/calculator.svg ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"calculator-usage\",\n      viewBox: \"0 0 64 64\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#calculator-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/calculator.svg?");

/***/ }),

/***/ "./assets/images/icons/check.svg":
/*!***************************************!*\
  !*** ./assets/images/icons/check.svg ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"check-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#check-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/check.svg?");

/***/ }),

/***/ "./assets/images/icons/close.svg":
/*!***************************************!*\
  !*** ./assets/images/icons/close.svg ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"close-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#close-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/close.svg?");

/***/ }),

/***/ "./assets/images/icons/cloth.svg":
/*!***************************************!*\
  !*** ./assets/images/icons/cloth.svg ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"cloth-usage\",\n      viewBox: \"0 0 64 64\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#cloth-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/cloth.svg?");

/***/ }),

/***/ "./assets/images/icons/delivery.svg":
/*!******************************************!*\
  !*** ./assets/images/icons/delivery.svg ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"delivery-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#delivery-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/delivery.svg?");

/***/ }),

/***/ "./assets/images/icons/discount.svg":
/*!******************************************!*\
  !*** ./assets/images/icons/discount.svg ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"discount-usage\",\n      viewBox: \"0 0 10 11\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#discount-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/discount.svg?");

/***/ }),

/***/ "./assets/images/icons/hearth.svg":
/*!****************************************!*\
  !*** ./assets/images/icons/hearth.svg ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"hearth-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#hearth-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/hearth.svg?");

/***/ }),

/***/ "./assets/images/icons/help.svg":
/*!**************************************!*\
  !*** ./assets/images/icons/help.svg ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"help-usage\",\n      viewBox: \"0 0 18 18\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#help-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/help.svg?");

/***/ }),

/***/ "./assets/images/icons/like.svg":
/*!**************************************!*\
  !*** ./assets/images/icons/like.svg ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"like-usage\",\n      viewBox: \"0 0 16 16\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#like-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/like.svg?");

/***/ }),

/***/ "./assets/images/icons/location.svg":
/*!******************************************!*\
  !*** ./assets/images/icons/location.svg ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"location-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#location-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/location.svg?");

/***/ }),

/***/ "./assets/images/icons/logo.svg":
/*!**************************************!*\
  !*** ./assets/images/icons/logo.svg ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"logo-usage\",\n      viewBox: \"0 0 140 82\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#logo-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/logo.svg?");

/***/ }),

/***/ "./assets/images/icons/more_h.svg":
/*!****************************************!*\
  !*** ./assets/images/icons/more_h.svg ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"more_h-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#more_h-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/more_h.svg?");

/***/ }),

/***/ "./assets/images/icons/more_v.svg":
/*!****************************************!*\
  !*** ./assets/images/icons/more_v.svg ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"more_v-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#more_v-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/more_v.svg?");

/***/ }),

/***/ "./assets/images/icons/online_card.svg":
/*!*********************************************!*\
  !*** ./assets/images/icons/online_card.svg ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"online_card-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#online_card-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/online_card.svg?");

/***/ }),

/***/ "./assets/images/icons/phone.svg":
/*!***************************************!*\
  !*** ./assets/images/icons/phone.svg ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"phone-usage\",\n      viewBox: \"0 0 18 18\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#phone-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/phone.svg?");

/***/ }),

/***/ "./assets/images/icons/point_delivery.svg":
/*!************************************************!*\
  !*** ./assets/images/icons/point_delivery.svg ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"point_delivery-usage\",\n      viewBox: \"0 0 18 18\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#point_delivery-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/point_delivery.svg?");

/***/ }),

/***/ "./assets/images/icons/pointdelivery.svg":
/*!***********************************************!*\
  !*** ./assets/images/icons/pointdelivery.svg ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"pointdelivery-usage\",\n      viewBox: \"0 0 18 18\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#pointdelivery-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/pointdelivery.svg?");

/***/ }),

/***/ "./assets/images/icons/search.svg":
/*!****************************************!*\
  !*** ./assets/images/icons/search.svg ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"search-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#search-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/search.svg?");

/***/ }),

/***/ "./assets/images/icons/sewingmachine.svg":
/*!***********************************************!*\
  !*** ./assets/images/icons/sewingmachine.svg ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"sewingmachine-usage\",\n      viewBox: \"0 0 64 64\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#sewingmachine-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/sewingmachine.svg?");

/***/ }),

/***/ "./assets/images/icons/shop.svg":
/*!**************************************!*\
  !*** ./assets/images/icons/shop.svg ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"shop-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#shop-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/shop.svg?");

/***/ }),

/***/ "./assets/images/icons/showroom.svg":
/*!******************************************!*\
  !*** ./assets/images/icons/showroom.svg ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"showroom-usage\",\n      viewBox: \"0 0 64 64\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#showroom-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/showroom.svg?");

/***/ }),

/***/ "./assets/images/icons/star.svg":
/*!**************************************!*\
  !*** ./assets/images/icons/star.svg ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"star-usage\",\n      viewBox: \"0 0 14 14\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#star-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/star.svg?");

/***/ }),

/***/ "./assets/images/icons/storecity.svg":
/*!*******************************************!*\
  !*** ./assets/images/icons/storecity.svg ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = {\n      id: \"storecity-usage\",\n      viewBox: \"0 0 18 18\",\n      url: __webpack_require__.p + \"./assets/images/icons-sprite.svg#storecity-usage\",\n      toString: function () {\n        return this.url;\n      }\n    }\n\n//# sourceURL=webpack:///./assets/images/icons/storecity.svg?");

/***/ }),

/***/ "./component/Footer/footer.scss":
/*!**************************************!*\
  !*** ./component/Footer/footer.scss ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./component/Footer/footer.scss?");

/***/ }),

/***/ "./component/Header/Header.js":
/*!************************************!*\
  !*** ./component/Header/Header.js ***!
  \************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _header_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header.scss */ \"./component/Header/header.scss\");\n/* harmony import */ var _header_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_header_scss__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _menu_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./menu.scss */ \"./component/Header/menu.scss\");\n/* harmony import */ var _menu_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_menu_scss__WEBPACK_IMPORTED_MODULE_1__);\n\n\n\n\n//# sourceURL=webpack:///./component/Header/Header.js?");

/***/ }),

/***/ "./component/Header/header.scss":
/*!**************************************!*\
  !*** ./component/Header/header.scss ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./component/Header/header.scss?");

/***/ }),

/***/ "./component/Header/menu.scss":
/*!************************************!*\
  !*** ./component/Header/menu.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./component/Header/menu.scss?");

/***/ }),

/***/ "./component/Tab/tab.js":
/*!******************************!*\
  !*** ./component/Tab/tab.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _tab_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tab.scss */ \"./component/Tab/tab.scss\");\n/* harmony import */ var _tab_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_tab_scss__WEBPACK_IMPORTED_MODULE_0__);\n\n\nconst tab = document.querySelector(\".tab\");\nconst btns = tab.querySelectorAll(\".tab__title\");\nconst items = tab.querySelectorAll(\".tab__item\");\nbtns.forEach(btn=>{\n  btn.addEventListener('click', (e)=>{\n    const {target} = e;\n    items.forEach(item=>{\n      if(target.dataset.target ===item.id){\n          item.classList.add(\"active\");\n          target.classList.add(\"active\");\n      }else{\n          item.classList.remove(\"active\");\n          btns.forEach(btn=>{\n            if(btn.dataset.target === item.id){\n              btn.classList.remove(\"active\");\n            }\n          })\n      }\n    })\n  });\n})\n\n\n\n//# sourceURL=webpack:///./component/Tab/tab.js?");

/***/ }),

/***/ "./component/Tab/tab.scss":
/*!********************************!*\
  !*** ./component/Tab/tab.scss ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./component/Tab/tab.scss?");

/***/ }),

/***/ "./modules/index.js":
/*!**************************!*\
  !*** ./modules/index.js ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _src_style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../_src/style.scss */ \"./_src/style.scss\");\n/* harmony import */ var _src_style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_src_style_scss__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _component_Header_Header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../component/Header/Header */ \"./component/Header/Header.js\");\n/* harmony import */ var _component_Tab_tab__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../component/Tab/tab */ \"./component/Tab/tab.js\");\n/* harmony import */ var _component_Footer_footer_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../component/Footer/footer.scss */ \"./component/Footer/footer.scss\");\n/* harmony import */ var _component_Footer_footer_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_component_Footer_footer_scss__WEBPACK_IMPORTED_MODULE_3__);\n\n\n\n\n\n\nfunction requireAll(r) {\n  r.keys().forEach(r);\n}\nrequireAll(__webpack_require__(\"./assets/images/icons sync recursive \\\\.svg$\"));\n\n\n//# sourceURL=webpack:///./modules/index.js?");

/***/ })

/******/ });