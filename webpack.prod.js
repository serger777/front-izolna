const webpack = require("webpack");
const merge = require("webpack-merge");
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const common = require("./webpack.common.js");

module.exports = merge(common, {
  mode: "production",
  // devtool: 'source-map',
  optimization: {
    minimizer: [
      new UglifyJSPlugin({
        parallel: true,
        cache: true,
        sourceMap: false,
        uglifyOptions: {
          parse: {
            ecma: 8
          },
          compress: {
            warnings: false,
            comparisons: false,
            ecma: 5
            // drop_console: true
          },
          mangle: {
            safari10: true
          },
          output: {
            comments: /\[AIV\]/,
            ascii_only: true,
            ecma: 5
          }
        }
      }),
      new OptimizeCSSAssetsPlugin({
        cssProcessor: require("cssnano"),
        cssProcessorPluginOptions: {
          preset: ["default", { discardComments: { removeAll: true } }]
        },
        canPrint: true
      })
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify("production")
    })
  ]
});
